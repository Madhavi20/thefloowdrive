package theFloow.Drive;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import utilities.Util;

public class TC_LoginAndLogout {
	static AndroidDriver<AndroidElement> driver;

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	static ExtentTest test;
	static com.relevantcodes.extentreports.ExtentReports reports;
	static File f = new File("src");
	static Properties p = new Properties();
	static Util util = new Util();
	static File fs = new File(f, ("Turnkey_88a599ba-d555-4b39-9a1a-c174b13ec1d2.apk"));

	@Test
	public void fetchReportDetails() throws Exception {
		AppLaunch();
		LoginApp();
		LogoutApp();
	}


	/************** Methods ****************/

	public static void AppLaunch() throws Exception {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		p.load(new FileInputStream(util.getPathOfCapabilities()));
		capabilities.setCapability("app", fs.getAbsolutePath());

		capabilities.setCapability("platformVersion", p.getProperty("platformVersion"));
		capabilities.setCapability("deviceName", p.getProperty("deviceName"));

		capabilities.setCapability("platformName", p.getProperty("platformName"));
		capabilities.setCapability("newCommandTimeout", p.getProperty("newCommandTimeout"));

		driver = new AndroidDriver<AndroidElement>(new URL(p.getProperty("url")), capabilities);
//		Thread.sleep(50000);
//		String reportsPath = "F:\\Eclipse\\framework\\Results\\result.html";
//		String projectPath = "F:\\Eclipse\\framework";
//		reports = ExtentConfigurations.getExtentInstance(reportsPath, projectPath, "TC_LoginAndLogout");
//
//		test = reports.startTest("TC_LoginAndLogout");

	}

	public static void LoginApp() throws InterruptedException {

//		Thread.sleep(5000);
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector()).scrollIntoView(text(\"Already registered\"));");
		// Thread.sleep(5000);
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/btn_already_registered").click();

		// driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/btn_already_registered").click();
		Thread.sleep(5000);
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/login_email").sendKeys("Abcd@xyz.com");
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/login_password").sendKeys("1234Abcd");
		Thread.sleep(5000);
		// driver.navigate().back();
		// driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/validation_group_login").click();

		driver.findElementByXPath("//android.widget.Button[@text='Login']").click();
		System.out.println("login btn");
		Thread.sleep(5000);
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/request_permissions_btn").click();
		System.out.println("perm btn");
		Thread.sleep(5000);
		driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		System.out.println("allow btn");
		Thread.sleep(5000);
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/skip_continue_btn").click();
		System.out.println("skip btn");
		Thread.sleep(5000);
		driver.findElementByXPath("//android.widget.Button[@text='Settings']").click();
		System.out.println("sett btn");
		driver.navigate().back();
		System.out.println("opt screen");
//		driver.findElementById("android:id/autofill_save_no").click();
//		System.out.println("save no btn");
		Thread.sleep(10000);
		driver.navigate().back();
		System.out.println("nav back");
	}

	public static void LogoutApp() throws InterruptedException {
		Thread.sleep(10000);
		driver.findElementByXPath("(//android.widget.ImageView)[3]").click();

		Thread.sleep(10000);
		System.out.println("nav logout");
		driver.findElementByXPath("//android.widget.TextView[@text='About']").click();
		Thread.sleep(5000);
		System.out.println("abt btn");
		driver.findElementByXPath("//android.widget.Button[@text='Log out']").click();
		Thread.sleep(5000);
		System.out.println("logout btn");
		driver.findElementByXPath("//android.widget.Button[@text='OK']").click();
		System.out.println("ok");
		Thread.sleep(5000);

	}

}
