package theFloow.Drive;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import utilities.ExtentConfigurations;
import utilities.Util;

public class TC_LaunchApp {
	static AndroidDriver<AndroidElement> driver;

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	static ExtentTest test;
	static com.relevantcodes.extentreports.ExtentReports reports;
	static File f = new File("src");
	static Properties p = new Properties();
	static Util util = new Util();
	static File fs = new File(f, ("Turnkey_88a599ba-d555-4b39-9a1a-c174b13ec1d2.apk"));

	//static DesiredCapabilitiesforAPP dc = new DesiredCapabilitiesforAPP();


	@Test
	public static void Run() throws Exception {

//		dc.Capabilities();
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		p.load(new FileInputStream(util.getPathOfCapabilities()));
		capabilities.setCapability("app", fs.getAbsolutePath());
		Thread.sleep(50000);

		capabilities.setCapability("platformVersion", p.getProperty("platformVersion"));
		capabilities.setCapability("deviceName", p.getProperty("deviceName"));

		capabilities.setCapability("platformName", p.getProperty("platformName"));
		capabilities.setCapability("newCommandTimeout", p.getProperty("newCommandTimeout"));

		driver = new AndroidDriver<AndroidElement>(new URL(p.getProperty("url")), capabilities);
		Thread.sleep(50000);	
		
//		test = reports.startTest("TC_AppLaunch");

	}

	@AfterTest
	public void reporTeardown() {

		reports.endTest(test);
		reports.flush();

	}

}
