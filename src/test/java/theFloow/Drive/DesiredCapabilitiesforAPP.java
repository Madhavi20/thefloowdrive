package theFloow.Drive;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import utilities.Util;

public class DesiredCapabilitiesforAPP {

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	

	public void reportSetup() {
		htmlReporter = new ExtentHtmlReporter("extent.html");

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

	}

	public AndroidDriver<AndroidElement> Capabilities() throws Exception {

		AndroidDriver<AndroidElement> driver;

		File f = new File("src");
		Properties p = new Properties();
		Util util = new Util();

		p.load(new FileInputStream(util.getPathOfCapabilities()));
		File fs = new File(f, p.getProperty("APPName"));

		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability("app", fs.getAbsolutePath());

		capabilities.setCapability("platformVersion", p.getProperty("platformVersion"));
		capabilities.setCapability("deviceName", p.getProperty("deviceName"));

		capabilities.setCapability("platformName", p.getProperty("platformName"));
		capabilities.setCapability("newCommandTimeout", p.getProperty("newCommandTimeout"));

		driver = new AndroidDriver<AndroidElement>(new URL(p.getProperty("url")), capabilities);
		Thread.sleep(10000);

		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/register_email_address")
				.sendKeys("philip.jaffadia@gmail.com");
		driver.findElementByXPath("//android.widget.EditText[@text='Password']").sendKeys("Iamking1");
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/register_confirm_password")
				.sendKeys("Iamking1");
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/register_forename").sendKeys("Philip");
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/register_surname").sendKeys("Jhonson");
		// dob

		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector()).scrollIntoView(text(\"I accept the terms of use\"));");
		driver.findElementByClassName("android.widget.CheckBox").click();
		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/register_postcode").sendKeys("2356");

		driver.findElementById("com.thefloow.thefloowltd.floowdrive.app:id/btn_register").click();

		Thread.sleep(50000);
		return driver;

	}
}