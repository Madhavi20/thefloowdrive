package utilities;

import java.io.File;
import java.net.URL;

public class Util {
	public File getPathOfCapabilities() {
		URL url = getClass().getResource("AppProperties.properties");
		File file = new File(url.getPath());
		return file;
	}
		
	/*	public File getPathOfLocators() {
			URL url1 = getClass().getResource("Locators.properties");
			File file1 = new File(url1.getPath());
			return file1;
	}*/
}
