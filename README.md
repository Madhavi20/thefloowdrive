This is the framework of appium for testing the "Floow Drive" mobile app.
Two test cases:
a) Launching the Floow Drive app.
b) Logging in and logging out 
functionalities of this app are covered.
This project is created in java using appium and TestNG frameworks.

Can check the TestNG test reports in the test-output folder.

The two test cases are:
a) TC_LaunchApp.java
b) TC_LoginAndLogout.java

Desired Capabilities for the app are being taken from the "AppProperties.properties" folder which is located in the "resources" folder.

The apk file is present in the "src" folder.